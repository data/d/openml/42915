# OpenML dataset: chemical-ceramic

https://www.openml.org/d/42915

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Ziyang He, Maolin Zhang, Haozhe Zhang
**Source**: [UCI](https://archive.ics.uci.edu/ml/datasets/Chemical+Composition+of+Ceramic+Samples) - 2019
**Please cite**: [Paper](https://arxiv.org/pdf/1511.07825.pdf)  

**Chemical Composition of Ceramic Samples Data Set**

The energy dispersive X-ray fluorescence (EDXRF) was used to determine the chemical composition of celadon body and glaze in Longquan kiln (at Dayao County) and Jingdezhen kiln. Forty typical shards in four cultural eras were selected to investigate the raw materials and firing technology. We hope to identify chemical elements that are strongest explanatory variables to classify samples into different cultural eras and kilns.


### Attribute information

- Ceramic.Name: name of ceramic types from Longquan and Jindgezhen 
- Part: a binary categorical variable ('Body' or 'Glaze') 
- Na2O: percentage of Na2O (wt%) 
- MgO: percentage of MgO (wt%) 
- Al2O3: percentage of AI2O3 (wt%) 
- SiO2: percentage of SiO2 (wt%) 
- K2O: percentage of K2O (wt%) 
- CaO: percentage of CaO (wt%) 
- TiO2: percentage of TiO2 (wt%) 
- Fe2O3: percentage of Fe2O3 (wt%) 
- MnO: percentage of MnO (ppm) 
- CuO: percentage of CuO (ppm) 
- ZnO: percentage of ZnO (ppm) 
- PbO2: percentage of PbO2 (ppm) 
- Rb2O: percentage of Rb2O (ppm) 
- SrO: percentage of SrO (ppm) 
- Y2O3: percentage of Y2O3 (ppm) 
- ZrO2: percentage of ZrO2 (ppm) 
- P2O5: percentage of P2O5 (ppm)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42915) of an [OpenML dataset](https://www.openml.org/d/42915). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42915/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42915/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42915/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

